# eureka service discovery server

#### Note

```
1. Eureka is Client Side Service Discovery.
```

#### Imp file


```
1. pom.xml
2. EurekaServiceDiscoveryApplication
3. application.yml
4. application-local.yml
```

#### Steps

```
s1 -  Start spring boot app (EurekaServiceDiscoveryApplication).
s2 - once its stated visit  http://127.0.0.1:8761/
S3 - There we will see all the restered services(if any)
```